<div class="collapse navbar-collapse">
	<ul class="nav navbar-nav navbar-right">
		<li><?php echo $this->Html->link(__('HOME'), array('plugin'=>null, 'controller'=>'pages', 'action' => 'display', 'home'));?></li>
		<li><?php echo $this->Html->link(__('TOUR'), array('plugin'=>null, 'controller'=>'pages', 'action' => 'display', 'tour'));?></li>
		<li><a href="#">NEWS</a></li>
		<!--<li><a href="#team">PARTNERS</a></li>-->
		<li><?php echo $this->Html->link(__('ABOUT'), array('plugin'=>null, 'controller'=>'pages', 'action'=>'about'));?></li>
		
		<?php 
		/*if ($showAddEventsButton) {  // has permissions to add events
			echo $this->Html->link(__('Add an event'), array(
				'plugin'=>null,
				'controller'=>'events',
				'action'=>'add'
			), array('rel'=>'nofollow', 'id'=>"add-event-box")); 
		}*/
		/*$this->set('title_for_layout', __('users login'));*/
		if ($this->Session->read('Auth.User')) { // registered user
			$nombre = $this->Session->read('Auth.User.firstname').' '.$this->Session->read('Auth.User.lastname');
			echo '<li>'.$this->Html->link($nombre, array(
				'admin'=>false,
				'plugin'=>null,
				'controller'=>'users',
				'action'=>'view',
				$this->Session->read('Auth.User.username')),
				array('style' => 'color:#458281;')).'</li>';

			/*if (isset($showAdminButton) && $showAdminButton == true) { // has administrator permissions
				echo $this->Html->link(__('Admin'), array(
					'plugin'=>null,
					'controller'=>'events', 
					'action'=>'index',
					'admin'=>true
				));
			}*/

			echo '<li>'.$this->Html->link(__('Logout'), array(
				'admin'=>false,
				'plugin'=>null,
				'controller'=>'users',
				'action'=>'logout'
			)).'</li>';
			if($this->Session->read('Auth.User.photo')=='user_photo.jpg'){
			echo $this->Html->image('users/'.$this->Session->read('Auth.User.photo'));} 
			else {
			echo $this->Html->image('users/'.$this->Session->read('Auth.User.photo'), array('class'=>'imground'));}
		} else { // anonymous user
			/*echo $this->Html->link(__('Login'), array(
				'admin'=>false,
				'plugin'=>null,
				'controller'=>'users',
				'action'=>'login'
			));

			if (Configure::read('evento_settings.adminAddsUsers')==false) { // users registration is allowed
				echo $this->Html->link(__('Register'), array(
					'admin'=>false,
					'plugin'=>null,
					'controller'=>'users',
					'action'=>'register'
				));
			}*/
			echo '<li><a href="'.$this->Html->url(array('controller' => 'users', 'action' => 'register')).'"><button class="btn btn-outline-success">JOIN</button></a></li>';
		}
		//echo '</div>';
		?>
	</ul>
</div>