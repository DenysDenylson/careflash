	<div id="userlogedhead">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-11 col-sm-4 tittle" style="color:#74568c;">My Feed Page</div>
			</div>
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-11 col-sm-4 subtittle" style="color:#4a4a4a;">Keeping you updated</div>
			</div>
		</div>			
	</div>
	<div id="myfeedpagecontent">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 col-sm-10 wow fadeIn" data-wow-delay="0.3s">
					<?php 
						if(!empty($user['Careo'])) {
							foreach($user['Careo'] as $careopoli){
								$id = $careopoli['CareopolisUser']['careopoli_id'];
								$careopolitem = $this->requestAction('careopolis/obtencareopoli/'.$id);
								//echo $careopolitem;
							?>
							<div class="careopoli">	
								<div class="row">
									<div class="col-md-4 col-sm-4">
										<div class="border-car base-with-shadow-car"><img src="<?php echo $this->Html->url('/img/icon/col-3-careopolis-main-image.png'); ?>" class="img-responsive"/></div>
									</div>
									<div class="col-md-8 col-sm-8 margeizq">
										<div class="row">
											<div class="h3-careopolis_name col-md-6 col-sm-6"><?php echo $careopolitem[0]['Careopoli']['title'];?></div>
											<div class="h3-careopolis_name col-md-6 col-sm-6" style="text-align:right">
											<a href="<?php echo $this->Html->url(array('controller' => 'careopolis', 'action'=>'view', $careopolitem[0]['Careopoli']['title'])); ?>" class="btn btn-primary bnt-create-care" role="button">Visit Careopolis</a>
											</div>
										</div>
										<div class="col-7-p-text"><?php echo $careopolitem[0]['Careopoli']['content'];?></div>
										<div class="options">
											<div class="row">
												<div class="col-md-4 col-sm-4">
													<img src="<?php echo $this->Html->url('/img/icon/myfeed_discussion.png'); ?>"/><span class="title">Discussion</span>
												</div>
												<div class="col-md-8 col-sm-8 section_activity">
													<?php 
													$threads = $this->requestAction('threads/obtenthreads/'.$careopolitem[0]['Careopoli']['boardid']);
													if(!empty($threads)){ 
														foreach($threads as $thread){
														$author = $this->requestAction('users/obtenmember/'.$thread['Thread']['author']);?>
														<div class="link"><?php  echo $this->Html->link($thread['Thread']['title'],array('controller' => 'careopolis','action' => 'view',$careopolitem[0]['Careopoli']['title']));?></div>
														<div class="info"><?php $date=date_create($thread['Thread']['createDate']);
														echo 'was created by <strong>'.ucwords($author[0]['User']['firstname']).' '.ucwords($author[0]['User']['lastname']).'</strong> '.date_format($date,'\o\n M\, j g:i a 	');?></div>
													<?php } } else { ?>
														<div class="infonot">No update available</div>
													<?php } ?>
													<div class="divider"></div>
												</div>						
											</div>
										</div>
										<div class="options">
											<div class="row">
												<div class="col-md-4 col-sm-4">
													<img src="<?php echo $this->Html->url('/img/icon/myfeed_photos.png'); ?>"/><span class="title">Photos</span>
												</div>
												<div class="col-md-8 col-sm-8 section_activity">
													<?php if($careopoli['CareopolisUser']['careopoli_id'] == "Joe Morgan") { ?>
														<div class="link"><?php echo $careopoli['CareopolisUser']['careopoli_id'];?></div>
														<div class="info"><?php echo $careopoli['CareopolisUser']['careopoli_id'];?></div>
													<?php } else { ?>
														<div class="infonot">No update available</div>
													<?php } ?>
													<div class="divider"></div>
												</div>
											</div>
										</div>
										<div class="options">
											<div class="row">
												<div class="col-md-4 col-sm-4">
													<img src="<?php echo $this->Html->url('/img/icon/myfeed_calendar.png'); ?>"/><span class="title">iHelp Calendar</span>
												</div>
												<div class="col-md-8 col-sm-8 section_activity">
													<?php if($careopoli['CareopolisUser']['careopoli_id'] == "Joe Morgan") { ?>
														<div class="link"><?php echo $careopoli['CareopolisUser']['careopoli_id'];?></div>
														<div class="info"><?php echo $careopoli['CareopolisUser']['careopoli_id'];?></div>
													<?php } else { ?>
														<div class="infonot">No update available</div>
													<?php } ?>
													<div class="divider"></div>
												</div>
											</div>
										</div>
										<div class="options">
											<div class="row">
												<div class="col-md-4 col-sm-4">
													<img src="<?php echo $this->Html->url('/img/icon/myfeed_story.png'); ?>"/><span class="title">Storytelling</span>
												</div>
												<div class="col-md-8 col-sm-8 section_activity">
													<?php if($careopoli['CareopolisUser']['careopoli_id'] == "Joe Morgan") { ?>
														<div class="link"><?php echo $careopoli['CareopolisUser']['careopoli_id'];?></div>
														<div class="info"><?php echo $careopoli['CareopolisUser']['careopoli_id'];?></div>
													<?php } else { ?>
														<div class="infonot">No update available</div>
													<?php } ?>
													<div class="divider"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	
							<?php } 
						}
						//echo $this->element('careopoli_item', array('careopolis' => $careopolis)); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
	
		
