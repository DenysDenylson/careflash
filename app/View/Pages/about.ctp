<script>
	  $( function() {
		$( "#tabs" ).tabs();
	  } );
</script>
<script>
  $( function() {
	$( "#accordion1" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
	$( "#accordion2" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
	$( "#accordion3" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
	$( "#accordion4" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
	$( "#accordion5" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
	$( "#accordion6" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
	$( "#accordion7" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
	$( "#accordion8" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
	$( "#accordion9" ).accordion({
      heightStyle: "content",
	  collapsible: true,
      active: false,
    });
  } );
</script>
	<!-- start about head -->
		<div id="aboutpagehead">
			<div class="container">
				<div class="row">
					<div class="tittle">Community when it matters most</div>
				</div>
			</div>			
		</div>
		<div id="aboutpagecontent">		
			<div class="container">
			<div id="tabs">
				<ul>
					<li><a href="#homeabout">ABOUT</a></li>
					<li><a href="#faqabout">FAQ</a></li>
					<li><a href="#teamabout">TEAM</a></li>
					<li><a href="#contactabout">CONTACT US</a></li>
				</ul>
				<div id="homeabout">
						<div class="row">
							<div class="col-md-7 col-sm-6 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">
								<div class="col-6-h3-title">Inspiration behind CareFlash</div>
								<div class="col-6-p-text spaceleft">
									<p>CareFlash is located in Austin, TX and was founded in 2006 by Jay Drayer out of an experience he had when a family member was diagnosed with a chronic health challenge where complications developed... culminating in several weeks in hospice and a death.</p></br>
									<p>Testifying to the ways positive transformation can be driven by tragic events, the experience that motivated Jay to depart his career in corporate finance surrounded the 9/11 tragedies in which he witnessed the second plane's impact on the World Trade Center while on his way to a 9:00 meeting that morning at 60 Wall St.</p>
								</div>
								<div class="col-6-h3-title">What does CareFlash do?</div>
								<div class="col-6-p-text spaceleft">
									<p>This is not another patient website that engages loved ones with nonstop updates, well wishes and always asking how they can help - but a complete approach that moves loved ones from sympathy to empathy... enabling them from near and far to more naturally and comfortably rally 'round - extending integrated care and strengthening outlook and peace of mind involving circumstances such as:</p></br>
									<div class="col-md-6 col-sm-6">
										<div class="feature-list">
										  <ul>
											<li>Illness</li>
											<li>Injury</li>
											<li>Childbirth</li>
											<li>Aging</li>
										  </ul>
										</div>
									  </div>
									  <div class="col-md-6 col-sm-6">
										<div class="feature-list">
										  <ul>
											<li>Cognitive decline</li>
											<li>Behavioral challenge</li>
											<li>Addiction recovery</li>
											<li>Grief from a loss</li>
										  </ul>
										</div>
									  </div>
								</div>
							</div>
							<div class="col-md-5 col-sm-6 wow fadeInRight" data-wow-delay="0.3s">
								<div class="bg-highlighta">
									<div class="col-5-p-text-highlight">A Careopolis is entirely (and quickly and easily) created and operated by the family and requires no education or training.</div>
								</div>
								<div class="col-6-p-text">
									<p><strong>CareFlash</strong> also partners with hundreds of providers nationwide - enhancing comprehensive care, reducing caregiver burnout, improving overall wellness and enhancing marketing as in this sample video:</p>
								</div>
								<div class="video-container">				  
									<video width="475" height="268" controls poster="<?php echo $this->Html->url('/img/image-video-cover.jpg'); ?>"><source src="<?php echo $this->Html->url('/media/Careopolis and Social Healing.mp4'); ?>" type="video/mp4"></video>
								</div>
							</div>
						</div>
				</div>
				<div id="faqabout">
					<div class="col-6-h3-title">Frequently Asked Questions</div></br></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">General questions about CareFlash</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion1">
									<p class="col-7-h5-question">What's the general idea behind CareFlash?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">CareFlash is service that facilitates "Community" among friends and family in ways that drive better outcomes in experiences including:</p>
											<ul>
												<li>Injuries or illnesses</li>
												<li>Wellness challenges</li>
												<li>Eldercare</li>
												<li>Deployed military members</li>
												<li>Reintegrating veterans</li>
												<li>Natural disasters</li>
												<li>Hospice</li>
												<li>Moving forward after a death</li></ul>
										<p class="col-7-text-answer">Dealing with circumstances such as these can be unsettling. Your family is special with its unique bonds. Create your own private online Careopolis(s) that help families work through difficult challenges and decisions surrounding caring for loved ones in ways that enhance quality of life and nurture better outcomes.</p>
										<p class="col-7-text-answer">Communities help family care providers maintain balance in their lives while engaging the most important people around a loved one. Here are some of the features that make it work:</p>
											<ul>
												<li>A private blog for communicating recovery updates, well wishes and meaningful dialogues</li>
												<li>An interactive iHelp calendar for tastefully but unobtrusively engaging much-needed assistance from friends with tasks, errands, meals and much else</li>
												<li>Rich spiritual and educational information including plainly-narrated 3-D health and wellness animations</li>
												<li>A photo page, for sharing and re-living warm memories together</li>
												<li>Communities are easily set-up in 2-3 minutes, requiring no technical training</li></ul>
									</div>
									<p class="col-7-h5-question">Why is a Careopolis helpful?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Careopolis(s) go miles to reduce stress and anxiety for family care providers. The results - family care providers are more productive and the entire support community is more effective without feelings of intrusiveness. Careopolis(s) also provide avenues to more easily keep family and friends in the loop on what's going-on, while engaging their involvement... all of which are proven to reduce depression and anxiety, drive better outcomes and speed recoveries.</p>
									</div>
									<p class="col-7-h5-question">Does CareFlash provide medical advice and opinions?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">No. CareFlash offers no medical opinions or advice. We are not knowledgeable about healthcare... but are former business executives who labor passionately every day to make a difference in people's lives by alleviating fear and anxiety via "Community".</p>
									</div>
									<p class="col-7-h5-question">an I create more than one Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Yes. Once you are signed-up as a registered user, create as many Careopolis(s) as you would like.</p>
									</div>
								</div>
							</div>
					</div></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">The 3-D animations in my Careopolis</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion2">
									<p class="col-7-h5-question">Who produces the animations hosted on CareFlash?</p>
										<div class="bg-answer-text-20">
											<p class="col-7-text-answer">The animations you see on CareFlash are produced by Blausen Medical Communications, a world-class medical animation and illustration company with the largest and most comprehensive library of proprietary medical imagery in the world. (Blausen.com)</p>
										</div>
									<p class="col-7-h5-question">How can I download the 3-D video animations?</p>
										<div class="bg-answer-text-20">
										<p class="col-7-text-answer">The 3-D Animation aren't available to download, you just can view or link to them.</p>
									</div>
								</div>
							</div>
					</div></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">Email accounts and passwords</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion3">
									<p class="col-7-h5-question">How can I change the email address that I use in my CareFlash account?</p>
										<div class="bg-answer-text-20">
											<p class="col-7-text-answer">Open Instructions</p>
										</div>									
									<p class="col-7-h5-question">If I forgot my password, how can I reset it?</p>
										<div class="bg-answer-text-20">
											<p class="col-7-text-answer">In order to reset your password, please visit https://careflash.com</p>
												<ul>
													<li>Click on "Forgot password"</li>
												<li>Fill in your username or email address</li>
													<li>Shortly, you will receive an email providing you a new password</li>
												<li>Please use care because the password you receive is CaSeSensiTiVE</li>
												</ul>		
										</div>	
									<p class="col-7-h5-question">If my password does not seem to be working, what should I do?</p>
										<div class="bg-answer-text-20">
											<p class="col-7-text-answer">If you have issues logging into CareFlash, there is a good likelihood that you have mistyped either:</p>
												<ul>
													<li>Your email account/user name or</li>
													<li>Your password</li>
												</ul>		
											<p class="col-7-text-answer">Passwords are case-sensitive so please ensure you are correctly entering the required characters. If all else fails, please reset your password by visiting  https://careflash.com</p>
												<ul>
													<li>Click on "Forgot password"</li>
													<li>Fill in your username or email address</li>
													<li>Shortly, you will receive an email providing you a new password</li>
													<li>Please use care because the password you receive is CaSeSensiTiVE</li>
												</ul>	
										</div>	
									<p class="col-7-h5-question">How can I change my password?</p>
										<div class="bg-answer-text-20">
											<p class="col-7-text-answer">In order to change your password, please visit https://careflash.com</p>
												<ul>
													<li>Log into your account</li>
													<li>Click on the "My CareFlash" tab near the top of the page</li>
													<li>Click on the "My Account" link on the turquoise bar</li>
													<li>Click "Change Password"</li>
													<li>Enter and confirm your new password</li>
													<li>Click "Update Password"</li>
												</ul>		
										</div>	
								</div>
							</div>
					</div></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">Setting-up and deleting Careopolis(s)</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion4">
								<p class="col-7-h5-question">Who can set-up a Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Anyone can create a user account on CareFlash and create a Careopolis. Friends, loved ones, clergy members and work associates can enhance the process at hand by creating a Careopolis and getting people engaged and involved. Please remember to use discretion and good taste in what you post in a Careopolis.</p>
									</div>	
								<p class="col-7-h5-question">How time-consuming is it to create a Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Setting up a Careopolis typically takes between 2-3 minutes and requires no technical training.</p>
									</div>
								<p class="col-7-h5-question">How do I set up a Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">To set up a Careopolis, click on "Create a Careopolis". On the Careopolis creation webpage, there is a clickable link that shows in clear terms how easy it is to create a Careopolis.  Open Instructions</p>
									</div>
								<p class="col-7-h5-question">Can I delete my Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Yes, you can delete any Careopolis where you are the owner (administrator) of that Careopolis.  If you wish to do so, follow these steps:</p>
											<ul>
										<li>Log into CareFlash</li>
											<li>Navigate to the Careopolis you wish to delete</li>
											<li>Click on Edit Careopolis</li>
										<li>At the bottom of the page, click "Delete Careopolis"</li></ul>
										<p class="col-7-text-answer">Please be sure this is what you intend to do because once you delete a Careopolis... it is gone  forever. </p>
									</div>
								</div>
							</div>
					</div></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">Adding and removing members from Careopolis(s)</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion5">
								<p class="col-7-h5-question">How do I invite friends to join a Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Invite people to participate and interact with you and the other members of your Careopolis.</p>
										<p class="col-7-text-answer">Open Instructions</p>		
									</div>
								<p class="col-7-h5-question">How do I invite others to become members in a Careopolis that I own?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">On the home page of the respective Careopolis, click "Invite friends". Once you have entered the names and email addresses of invitees, CareFlash automatically launches email invitations to these invitees.</p>
									</div>
								<p class="col-7-h5-question">Can anyone find my Careopolis by using a search engine?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">No. Careopolis(s) are hidden from search engines.</p>
									</div>
								<p class="col-7-h5-question">Who has the authority to invite new members into a Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Only the owner (or Administrator) of each Careopolis has this power to invite new members into that Careopolis.</p>
									</div>
								<p class="col-7-h5-question">Can a member within a given Careopolis (someone other than the owner/administrator) invite someone to become a member of that same Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">No, but that member can "nominate" someone to be invited into the Careopolis by the owner.(See the next entry.)</p>
									</div>
								<p class="col-7-h5-question">What if a member (who is not the owner or administrator of a particular Careopolis) invites others into that Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Only the owner (administrator) of a Careopolis can launch invitations to prospective members. If a member of a Careopolis (who is not the owner or administrator) wishes to nominate someone to join into a Careopolis, they can accomplish this by navigating to the home page of the particular Careopolis and:</p>
											<ul>
											<li>Click "Invite friends"</li>
											<li>Entering the name and email address of the nominee and click "Invite"</li>
										<p class="col-7-text-answer">CareFlash then automatically submits a nomination request via email to the owner of that Careopolis. The owner has the option of accepting or rejecting the nomination. Assuming the owner affirms the nomination, the nominee will automatically receive an email notification from CareFlash inviting them to join into that Careopolis. The owner of the Careopolis also has the ability to ask the nominator for more information on who the nominee is... and as well can simply reject the nomination. Bear in mind that rejections are not necessarily punitive but may simply indicate that the owner feels that the person's involvement may not be appropriate given:</p>
											<li>The level of dialogue transpiring in the Careopolis</li>
											<li>The group of people that have been assembled (close family members and friends)</li></ul>		
									</div>
								<p class="col-7-h5-question">How do I remove members from a Careopolis or promote members to moderator status?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Only the administrator/owner of a given Careopolis has the power to promote members to moderator status, or to remove members from that Careopolis. In 'View all friends' (lower left side), click Edit next to a member's name and a box will appear giving you the option to either Make moderator or Remove user.</p>
										<p class="col-7-text-answer">Open Instructions</p>
									</div>
								<p class="col-7-h5-question">How can I Invite Users from other Careopolis(s)?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">In Careopolis Members we have the options to "Invite Users from other Careopolis(s)".</p>
										<p class="col-7-text-answer">Open Instructions</p>
									</div>
								<p class="col-7-h5-question">How to locate a Careopolis using Search and requesting an invitation to join into it?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">People can locate a Careopolis and join it through the "Search" option</p>
										<p class="col-7-text-answer">Open Instructions</p>
									</div>
								</div>
							</div>
					</div></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">Control of Careopolis(s), administrator details, etc.</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion6">
								<p class="col-7-h5-question">Can I delegate authority to another CareFlash user, allowing them to serve as co-administrator of a Careopolis that I have created?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Yes. In doing so, the new co-administrator (a moderator) will have all the "authority" of an administrator.</p>
									</div>
								<p class="col-7-h5-question">Do I have the ability to modify information in a Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">As the owner/administrator of a Careopolis, you have the ability to modify anything on the Careopolis. If you are a member of a Careopolis, you do not have the ability to modify updates or most other details.</p>
									</div>
								<p class="col-7-h5-question">Can I delete a Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Yes, but only the owner of that given Careopolis has the power to delete a given Careopolis. On the home page of the Careopolis, click 'Edit this Careopolis' (top left). On the bottom of the "Edit your private Careopolis" page, click the red button titled 'Delete this Careopolis'. You will be prompted to confirm that you want to proceed with the deletion.</p>
										<p class="col-7-text-answer">Open Instructions</p>
									</div>
								<p class="col-7-h5-question">When I post updates to my Careopolis, do members receive notifications?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Yes, when you post an update to your Careopolis blog, each member is notified via email that the update is available for them to read when they are ready.</p>
									</div>
								<p class="col-7-h5-question">What does the email notification look like?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">The notification is a shortened version of the update. It contains a link which can be clicked-on to view the entire update, and reply to it. CareFlash is more of a community solution and our objective is less-so aimed at communicating recovery updates, and more-so aimed at better engaging the involvement of the members in driving better outcomes, reducing aloneness and depression, and so on.</p>
									</div>
								<p class="col-7-h5-question">Do email updates need to be retrieved immediately?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">No. Updates can be retrieved at any time.</p>
									</div>
								<p class="col-7-h5-question">Are there limitations on how frequently updates can be entered?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">No, there are no limitations.</p>
									</div>
								<p class="col-7-h5-question">Can a "Friend" post a response to an update?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Yes, and it will be seen by all Careopolis members.</p>
									</div>
								<p class="col-7-h5-question">What other tools are provided by CareFlash?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">In addition to the updates tool, CareFlash provides users with the ability to post photos and videos, request and organize help via the iHelp Calendar, access meaningful spiritual content unique to that visitor's personal preferences and research information on diseases or treatments.</p>
									</div>
								</div>
							</div>
					</div></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">Photo page in each Careopolis</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion7">
								<p class="col-7-h5-question">May I post photos into a Careopolis?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">Yes, simply choose the photos tab, select the photo you want to share with others, and upload it into your Careopolis.</p>
									</div>
								<p class="col-7-h5-question">Can I share my photos?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">You can share your photos with the members of your Careopolis. Any photos you post will be seen by all members of the Careopolis</p>
									</div>
								</div>
							</div>
					</div></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">iHelp calendar</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion8">
								<p class="col-7-h5-question">How do I let my members in my Careopolis know how and when help is needed?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">The iHelp Calendar(TM) provides an easy, unobtrusive way for you to post your needs on an interactive calendar. Click to have CareFlash automatically launch emails to your Careopolis when you post something. As members volunteer to take responsibility for tasks, their acknowledgement is noted for everyone's benefit... reducing the redundancy that is so common. Areas of assistance that are commonly seen include:</p>
								<ul>
								<li>Meals, tasks and errands</li>
								<li>Childcare or pet care</li>
								<li>Lawn care and assistance around the house</li>
								<li>Suggested times for visitations (and notes for when the time for a visit is not opportune - like during therapy, treatments, etc.)</li>
								<li>Schedule an event or outing</li>
								<li>Schedule important recurring events (like continuing therapy dates or even birthdays and anniversaries)</li></ul>
									</div>
								<p class="col-7-h5-question">Who can add events to my iHelp calendar?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">All members of the Careopolis can add events in the calendar by clicking on the (+) symbol or in "Add an iHelp Calendar(TM) Event" in the calendar. Be sure to check the box if you would like CareFlash to email members, notifying them that a request has been made.</p>
										<p class="col-7-text-answer">Open Instructions</p>
									</div>
								</div>
							</div>
					</div></br>
					<div class="Hr-divider"></div></br>
					<div class="row">
							<div class="col-md-5 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">								
								<div class="col-4-h4-topic-title">Careopolis Blog</div>
							</div>
							<div class="col-md-7 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
								<div id="accordion9">
								<p class="col-7-h5-question">How can I post messages, photos, videos in the Careopolis blog?</p>
									<div class="bg-answer-text-20">
										<p class="col-7-text-answer">You can enrich your posts by adding multimedia content to them. In this quick guide we will show you how to add images and videos to your posts.</p>
										<p class="col-7-text-answer">Open Instructions</p>
									</div>
								</div>
							</div>
					</div></br>
				</div>
				<div id="teamabout">
					<div class="container">
						<div class="row">
							<div class="col-6-h3-title">ADVISORY BOARD</div>	
							<div class="col-6-h6-title">It's our people who make the difference</div>
							<div class="col-12-p-text">
								<p>CareFlash is inspired and run by passionate professionals, each with perspectives born out of personal experiences that provide them not only pride in what Careopolis(s) represent to people... but the empathy to appreciate how we enhance lives. Our backgrounds also encompass an energy and a deep passion for entrepreneurship within the ways we make a difference every day.</p></br>
								<p>Community. We've always assumed that people with more friends live longer, happier lives. As things continue to happen in life, study after study continues to validate that people with larger social networks have better outcomes and suffer less depression. Community is universal.</p>
							</div>
							<div class="col-6-h3-title">ADVISORY BOARD MEMBERS</div>
						</div>
						<div class="row">
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/jay-drayer.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Jay Drayer</div>
										<div class="divider"></div>
										<div class="overview-text">Jay is founder and CEO of CareFlash. Jay calls himself a "recovering CFO" who took lessons from 20 years of corporate finance and software industry experience and interwove them with experience as care provider to a family member who transitioned through intensive care and hospice care to a funeral.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"Our Careopolis(s) make a difference every day in the lives of thousands around the world."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
									  <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/samira-beckwith.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Samira Beckwith</div>
										<div class="divider"></div>
										<div class="overview-text">Samira is the President/CEO of Hope HealthCare Services and has been a dedicated champion for palliative care and end of life issues for over 30 years.  Her passion and expertise have been acknowledged throughout the State of Florida and nationally.  In recognition of her work, Florida Governor Jeb Bush described her as "a visionary who provides leadership on a local and national level - passionate about ensuring the highest quality of service."</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"I know that each day is a gift. Through exceptional care and support, it is my goal to help every individual and their loved ones as they fulfill life's journey.  Through CareFlash, Hope provides individuals with an effective communication tool for family and friends during difficult times.  This helps people share love, support, and Hope."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/colin-bester.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Colin Bester</div>
										<div class="divider"></div>
										<div class="overview-text">Colin is an established Chief Technology Officer with a longstanding reputation for success involving innovative business models and technical solutions that address global challenges.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"CareFlash has taken the step to leverage the power of social media in a controlled and unobtrusive way, providing a convenient and effective solution that has the potential to make a difference in people's lives."</div>
										</div>
									  </div>
								</div>
						</div>
						<div class="row">
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/bruse-blausen.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Bruce Blausen</div>
										<div class="divider"></div>
										<div class="overview-text">Bruce is Founder and CEO of Blausen Medical Communications, internationally recognized as the developer and owner of the world's largest 3D medical animation library.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"Whether using social media or 3D animations, CareFlash and Blausen share the common mission of alleviating fears and concerns of patients and their families."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
									  <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/judy-brizendine.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Judy Brizendine</div>
										<div class="divider"></div>
										<div class="overview-text">Judy is a sought-after speaker and blogger and the author of Stunned by Grief. As a result of a tragic loss, her mission and passion surround educating, encouraging and inspiring anyone facing grief to step into the powerful grieving process and move from the pain of loss to living expectantly again.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"Ever since Jay described CareFlash to me, I have remained intrigued by this innovation that brings people together in compassionate and meaningful ways in some of the most challenging, life-altering times in their lives - to nurture healing.  I'm honored participating among a community whose mission and passion are synonymous with positively impacting lives of people everywhere."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/david-lane-brown.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">David Lane Brown, M.D.</div>
										<div class="divider"></div>
										<div class="overview-text">David is a physician, board certified in child and adolescent psychiatry. Since 1994, Dr. Brown has cared for patients of all ages in private practice, in community mental health and residential treatment centers and within a psychiatric hospital where he served as both medical director and medical staff president.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">
												<p>"In January 2012, I heard the three most fear striking words I have experienced in life.  "You have cancer."  As a nonsmoker, I'd been diagnosed with advanced lung cancer which, let me tell you got my undivided attention."</p>
												<p>"As I worked develop some semblance of normalcy in my life, I did 3 things.  First, I embraced a holistic lifestyle, centric around mind, body and spirit.  Second, I started my own Careopolis to engage my friends and loved ones in my new identity as a cancer patient.  And third, I entered into a revolutionary clinical trial that uses targeted molecular pharmaceutical treatments to fight cancer.  I credit my holistic lifestyle, social network and medical care for the amazing (I can't bring myself to say miraculous) progress I'm making where we continue to witness therapeutic response that is wholly without precedent."</p>
												<p>"Jay and the CareFlash team have been responsive to my thoughts and now, through the numerous Careopolis(s) around the world, my observations continue to benefit patients and loved ones, as they work through life's most difficult challenges... empowered by the holistic benefits of community."</p>
											</div>
										</div>
									  </div>
								</div>
						</div>
						<div class="row">
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/elaine-shore-dorr.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Elaine Shore Dorr</div>
										<div class="divider"></div>
										<div class="overview-text">Elaine is an account executive with Caradigm, a Microsoft - GE Healthcare joint venture that leverages population risk to enhance medical provider success.  She has 20 years healthcare marketing expertise with Emdeon and Siemens Healthcare, and a passion to improve lives through health innovation and philanthropy.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"Jay has a history that inspires me in the ways his experiences speak to the strategic significance CareFlash holds for providers all along the entire care continuum.  My experience guides me to seek ever-better quality of care solutions via technology.  CareFlash has a proved track record in providing a broad spectrum, best of breed solution for individuals and loved ones dealing with health, wellness and aging challenges.  Careopolis(s) foster an unobtrusive, meaningful and supportive environment in ways that enable providers to be catalysts for positive change in the lives of those they serve."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
									  <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/mark-elder.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Mark Elder</div>
										<div class="divider"></div>
										<div class="overview-text">Mark is Director of Spiritual Care and Clinical Ethics with St. David's HealthCare in Austin, TX, and has many years of experience in and surrounding Social Work, Chaplaincy and Grief Support.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"All humans desire community. In community, we receive and give belonging, support and encouragement. CareFlash facilitates this level of community to people in ways that combine 21st century technology with the ages-old need for human compassion and care. I am excited to be part of an organization involved in these levels of engagement and connectedness among people."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/carole-fisher.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Carole Fisher</div>
										<div class="divider"></div>
										<div class="overview-text">Carole is the President/CEO of Nathan Adelson Hospice, Las Vegas' and Southern Nevada's oldest hospice.  Carole is both visionary and progressive in the tirless ways she couples her vast knowledge of healthcare and hospice, with a strong belief in the importance of compassion and empathy.  Earlier in Carole's career, she served as a Counselor and Executive Director of a respected substance abuse and behavioral care organization.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"During good times and bad, people need to know that they are not alone.  CareFlash is an innovative approach to using technology to enhance the depth, comfort and reassurance that is fostered through a sense of engaged community.  I applaud the innovation that CareFlash represents in the ways it encourages holistic healing."</div>
										</div>
									  </div>
								</div>
						</div>	
						<div class="row">
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/greg-grabowski.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Greg Grabowski</div>
										<div class="divider"></div>
										<div class="overview-text">Greg is a partner with Hospice Advisors. Previously, he was CEO of Seasons Hospice Foundation. Greg is internationally recognized for achievements he produces time and time again - successfully taking creative innovations from ideation through execution, in and surrounding healthcare and philanthropy.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"As in this photo of me with the famed Malcolm Gladwell whose gifts for critical thinking and direction serve as a beacon that inspires innovation, CareFlash is a revolution in whole healing - empowering people in collaborative ways throughout many of life's most challenging circumstances - all within innovation based on community."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
									  <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/ralph-hasson.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Ralph Hasson</div>
										<div class="divider"></div>
										<div class="overview-text">Ralph is the Chair of the Austin Board of Advisors for the Texas TriCities Chapter of NACD (National Association of Corporate Directors). He helped to build Chorda Conflict Management, Inc. from a startup into a nationally recognized consulting firm, and has been an advisor to Fortune 500's and startups alike.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"Because of Jay's personal commitment to the CareFlash mission, and because of the flexible crisis management platform CareFlash provides to families facing the toughest of situations."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/jason-hiles.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Jason Hiles</div>
										<div class="divider"></div>
										<div class="overview-text">Jason is Branch Manager of Home Instead Senior Care, Madison, WI. Previously, Jason was Director of Family Services with Hospice Advantage where he was responsible for Social Work, Chaplaincy, Grief Support and Volunteer Coordination across 65 offices nationwide.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"CareFlash's power resides in keeping families connected across miles. There is nothing stronger than the human connection. Keeping a family connected throughout a crisis is deeply profound and powerful. In addition to these capabilities, CareFlash keeps families connected after a crisis subsides... which is much more than memorable - it is habit changing."</div>
										</div>
									  </div>
								</div>
						</div>
						<div class="row">
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/patti-hill.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Patti Hill</div>
										<div class="divider"></div>
										<div class="overview-text">A recognized leader in a new generation of PR executives, Patti founded Penman PR, one of the most innovative independent public relations firms in the nation and the only international PR firm to offer 100% senior-level representation.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"In the face of social media networks designed for self-promotion, CareFlash is a platform that gives Careopolis members advantages in identifying efficient ways to have a meaningful impact in loved ones' lives.  It gives me great pleasure to be able to represent a company with such compassion and grace."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
									  <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/john-parker.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">John Parker</div>
										<div class="divider"></div>
										<div class="overview-text">John is responsible for Enterprise Data Management at Oracle. Educated in Computer Science at M.I.T, he has served as president and COO of a startup business, SVP of Operations for Knight Ridder Financial, and has over 20 years of sales experience with Merrill Lynch and Kidder Peabody.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"Relationships need to be cultivated and nurtured. Technology has the potential to isolate us, but it also has the potential to bring us together around common causes. CareFlash uses technology to foster real community by bringing together extended support networks, renewing friendships and providing aid and comfort to people in times of their most critical need."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/richard-scruggs.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Richard Scruggs</div>
										<div class="divider"></div>
										<div class="overview-text">Richard is a serial entrepreneur currently leading the start-up of Salient Pharmaceuticals Incorporated. His prior experience includes work in arenas diverse as information technology, manufacturing, agriculture and academia.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"I have been involved with CareFlash from its early days because of my interest in social media and projects that have the potential to change peoples' lives."</div>
										</div>
									  </div>
								</div>
						</div>
						<div class="row">
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/autumn-spence.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Autumn Spence</div>
										<div class="divider"></div>
										<div class="overview-text">Autumn is Sr. VP Business Development at Hospice Pharmacy Solutions, a leading hospice PBM provider.  Prior, she grew from being one of the original sales reps at Outcome Resources to become their VP Business Development where she was engaged for 12 years.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"I have been in healthcare and hospice for over a decade and have before never seen an innovation that excites industry professionals like CareFlash. Specifically in the ways it strengthens socialization and enhances the integrated approach to care, while empowering provider word of mouth and professional and community relations."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
									  <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/carlos-tirado.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Carlos Tirado, M.D., M.P.H., F.A.S.A.M.</div>
										<div class="divider"></div>
										<div class="overview-text">Carlos is a physician who is board certified in general psychiatry and addiction medicine. He is the medical director and director of clinical innovation at The Right Step Hill Country and chairs the clinical advisory panel for Sims Foundation which supports hundreds Austin musicians annually.  Dr. Tirado completed his M.D. and masters level training in the University of Texas System along with a fellowship at The University of Pennsylvania.  He continues to make his mark as an innovator in strategies for enhancing patient outcomes.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"As a practicing addiction medicine psychiatrist, I'm constantly scouring the landscape for innovations that enable success in the whole patient. By this, I'm referring to enhancing success and sustainability in addiction recovery and holistic living that is harmonious with career, family, community and society. CareFlash is exactly this sort of innovation and easily one of the most compelling social advances I have seen in many years, for equipping and empowering patients for their success."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/chris-wasden.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Chris Wasden</div>
										<div class="divider"></div>
										<div class="overview-text">Chris is Managing Director of Strategy and Innovation with PricewaterhouseCoopers.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"I see social media applications in healthcare as being transformational as they get more people engaged in changes in values and behaviors that will transform our healthcare system from one focused on sickness to one focused on health. CareFlash has the ability to improve the quality of care and increase the level of engagement beyond the physician and patient."</div>
										</div>
									  </div>
								</div>
						</div>
						<div class="row">
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
									 <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/dan-wilford.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">Dan Wilford</div>
										<div class="divider"></div>
										<div class="overview-text">Dan served as CEO of Texas' largest nonprofit healthcare system for 18 years, developing it into one of the nation's largest and most respected.  In addition to his many professional awards and achievements, Dan is a former NFL official and university level student athlete.  He serves on the board of directors of publicly traded LHC Group and Healthcare Realty Trust, along with numerous health-related not-for-profit organizations and in 2009 was inducted into Modern Healthcare's Hall of Fame.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"Having mentored Jay since 2005 when CareFlash was a simple concept drawn on a notepad, I'm consistently amazed by the enhancements his team makes to the Careopolis(s) that are strategic to the success of patients, caretakers, providers, insurers and to society."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
									  <div class="feature-itemabout">
										<div class="border base-with-shadow"><img src="<?php echo $this->Html->url('/img/david-wood.png'); ?>" class="img-responsive"/></div>
										<div class="member-name">David Wood</div>
										<div class="divider"></div>
										<div class="overview-text">David specializes in risk management and insurance solutions for the healthcare industry. In response to the crisis in patient safety in the healthcare system, David's firm, American Medical Risk LLC developed a new coverage called Patient Protect that insures patients from medical injuries received during treatment.</div>
										<div class="involvement-trigger-text">Why do I value my involvement with CareFlash?</div></br>
										<div class="bg-highlight">
											<div class="p-involvement-text">"I had a healthcare experience and saw CareFlash convert a widely dispersed community of loved ones into a well-orchestrated team. As much so with meals, errands and other logistical needs, but also with meaningful encouragement, respite care and much needed social engagement extending far beyond hospital discharge and through recovery and rehabilitation - all within a holistic healing Careopolis."</div>
										</div>
									  </div>
								</div>
								<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
								</div>
						</div>
					</div>
				</div>
				<div id="contactabout">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">
								<div class="col-6-h3-title">Contact information</div>
								<div class="row">
									<div class="col-md-11 col-sm-11">
									<div class="col-6-p-text">
										<p>For customer support inquiries, please e-mail <span class="text-style-1">support@careflash.com</span></p></br>
										<p>For sales inquiries, please e-mail <span class="text-style-1">info@careflash.com</span></p></br>
										<p>For administrative, partnership and corporate development inquiries, please e-mail <span class="text-style-1">ceo@careflash.com</span></p></br>
										<p>To contact us via US Mail, please write to:</p></br>
										<div class="text-style-2">
											<strong>
											<p>CareFlash, LLC </p>
											<p>1101 West 34th Street, Ste. 138</p>
											<p>Austin, TX 78705</p>
											</strong>
										</div>
									</div>
									</div>
									<div class="col-md-1 col-sm-1">
									</div>
								</div>	
							</div>
							<div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-delay="0.3s">
								<div class="col-4-h3-title">Feel free to drop us a message</div>
								<div class="container-login">					
										<form action="#" method="post" id="contact-form" role="form">
											<div><span style="padding-left:17px;">Your Name</span></div>
											<div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
												<input name="loginName" type="text" id="name" placeholder="Name">
											</div>
											<div><span style="padding-left:17px;">Your email</span></div>
											<div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.6s">
												<input type="password" name="password" value="" placeholder="Email">
											</div>
											<div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.9s">
											<div><span>Your Message</span></div>	
												<textarea name="message" rows="5" class="form-control" id="message" placeholder="Message"></textarea>
											</div>
											<div style="padding-left:17px;">
											  <input type="submit" class="btn btn-primary bnt-login" value="Send Message" />
											</div>
										</form>									
								 </div>
							</div>
						</div>
					</div>	
				</div>
			</div>
			</div>
		</div>
	
	
	