<?php
/*$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
*/?>
<!--<!DOCTYPE html>
<html>
<head>
	<?php //echo $this->Html->charset(); ?>
	<!--<title>
		<?php //echo $cakeDescription ?>:<?php //echo $this->fetch('title'); ?>
	</title>
	<title>CareFlash - Community when it matters most</title>-->
<!DOCTYPE html>
<html>
<head>
<?php
	echo $this->Html->charset();
	echo $this->Html->meta('icon');
	echo $this->Html->meta(array('name'=>'viewport', 'content'=>'width=device-width, initial-scale=1.0'));
	echo $this->Html->tag('title', ucwords($title_for_layout) . ' - ' . Configure::read('evento_settings.appName'));	
		
	//echo $this->Html->css('cake.generic');
	echo $this->Html->css(array('animate.min' ,'bootstrap.min', 'magnific-popup.min', 'font-awesome.min', 'style', 'jquery-ui'));
	echo $this->Html->script(array('jquery', 'jquery-ui'));

	// see default/elements/
	echo $this->Element('xml_feed_link');

	// Description meta tags. See View/Elements/meta_description.ctp
	echo $this->Element('meta_description');

	// CakePHP view blocks
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
	<script>
	  $( function() {
		$( "#tabs" ).tabs();
	  } );
	</script>
</head>
<body>
	<!-- start navigation -->
		<div class="navbar navbar-fixed-top navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand"><img src="<?php echo $this->Html->url('/img/logo.png'); ?>" class="img-responsive" alt="CareFlash - Communities when it matters most" width="240" height="60" /></a>
				</div>
				<?php echo $this->Element('menu');?>
			</div>
			<div class="top-bar">
				<div class="container">
					<div class="row">
						<?php if ($this->Session->read('Auth.User')) { ?>
						<div class="col-md-8 col-sm-4">
							<?php echo $this->Element('user_menu');?>
						</div>
						<?php } else { ?>
						<div class="col-md-8 col-sm-4"></div>
						<?php } ?>
						<div class="col-md-4 col-sm-4">
							<?php echo $this->Element('search');?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end navigation -->

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
	
	<footer class="footer text-center">
    <div class="container">
      <div class="row submenu">
        <div class="col-md-4 col-sm-4">
          <div class="footer-logo">
			<a href="#"><img src="<?php echo $this->Html->url('/img/logo-white.png'); ?>" class="img-responsive" alt="CareFlash - Communities when it matters most" width="240" height="60" /></a>
          </div>
        </div>
        <div class="col-md-8 col-sm-4">
          <div class="footer-menu">
              <span><a href="/contact">Contact Us</a></span> |
              <span><a href="/corp/tos">Terms of Use</a></span> |
              <span><?php echo $this->Html->link(__('Privacy'), array('plugin'=>null, 'controller'=>'pages', 'action' => 'display', 'privacy'));?></span> |
              <span><a href="mailto:support@careflash.com?subject=Tell Us&amp;body=Dear CareFlash,%0A">Tell Us!</a></span> |
              <span><a href="javascript:cf_links.tellfriend();">Tell A Friend About Us</a></span> |
              <span><a href="/faq">FAQs</a></span>
          </div>
        </div>
      </div>

      <div class="social-icon">
        <div class="col-md-2 col-sm-2"></div>
			<div class="col-md-8 col-sm-8">
				<h4 class="text-uppercase follow-tittle">FOLLOW US ON:</h4>
          <ul class="list-inline">
            <li class="list-inline-item">
				<a class="btn btn-outline-light btn-social text-center " href="http://www.facebook.com/careflash" target="_blank">
					<i class="fa fa-facebook"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center " href="http://twitter.com/CareFlash" target="_blank">
					<i class="fa fa-twitter"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center " href="http://www.youtube.com/careflashvideo" target="_blank">
					<i class="fa fa-youtube"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center " href="http://www.pinterest.com/careflash" target="_blank">
					<i class="fa fa-pinterest-p"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center" href="http://www.linkedin.com/company/474535" target="_blank">
					<i class="fa fa-linkedin"></i>
                </a>
            </li>
           </ul>
			</div>
		<div class="col-md-2 col-sm-2"></div>

      </div>
    </div>
  </footer>
  <div class="bg-footer-bottom">
    <div class="bottom-footer-text">
      &copy; 2018 CareFlash LLC. All rights reserved. Made with <img src="<?php echo $this->Html->url('/img/icon/heart-bottom-footer.png'); ?>"/> in Austin, TX.
    </div>
  </div>
		<!-- end footer -->
	<?php //echo $this->element('sql_dump');
	echo $this->Html->script(array('bootstrap.min', 'isotope', 'imagesloaded.min', 'wow.min', 'smoothscroll', 'jquery.flexslider', 'jquery.magnific-popup.min', 'custom'));?>
</body>
</html>
