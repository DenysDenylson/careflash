<?php
class Post extends AppModel
{
    public $name = 'Post';
    public $useTable = 'posts';
    /*public $actsAs = array('Containable',
        'Image' => array(
            'settings' => array(
                'titleField' => 'name',
                'fileField' => 'logo',
                'defaultFile' => 'event_logo.jpg', ),
            'photos' => array(
                'logo' => array(
                    'destination' => 'logos',
                    'size' => array('width' => 150, 'height' => 152), ),
                'small_logo' => array(
                    'destination' => 'logos/small',
                    'size' => array('width' => 75, 'height' => 75),
                ),
                'big_logo' => array(
                    'destination' => 'logos/big',
                    'size' => array('width' => 1200, 'height' => 280),
                ),
            ), ),
        );
*/
    // limit the events added with the repeat option to avoid server overload
    // 0 disables the limit check
  //  public $repeatLimit = 0;

    // limit repeat events to a number of months in the future
    // 0 disables the month limit check
   // public $repeatMonthsLimit = 0;

    /*
     * validation rules
     *

    public $validate = array(
        'name' => array(
            'rule' => array('minLength', 2),
            'message' => 'Name must be at least 2 characters long',
            'required' => true, ),
        'notes' => array(
            'rule' => array('minLength', 2),
            'message' => 'Please add some notes',
            'required' => true, ),
        'web' => array(
            'rule' => 'url',
            'message' => 'Please enter a valid web address.',
            'required' => false,
            'allowEmpty' => true, ),
        'end_date' => array(
            'rule' => 'checkEventDate',
            'message' => 'Please enter a valid end date',
            'required' => true, ),
        'category_id' => array(
            'rule' => 'numeric',
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Please select a category',
        ),
        'venue_id' => array(
            'rule' => 'validVenue',
            'message' => 'Please select a venue.',
            'required' => true,
            ),
        'recaptcha' => array(
            'notEmpty'    => array(
                'rule' => 'notBlank',
                'on' => 'create',
                'message' => 'Incorrect captcha',
                'required' => true, ),
            ),
        );*/

    /*
     * model asociations
     *

    public $hasAndBelongsToMany = array(
        'Tag' => array('className' => 'Tag'),
        'Attendees' => array(
            'className' => 'User',
            'unique' => false,
            'joinTable' => 'events_users',
            'foreignKey' => 'event_id',
            'asociationForeignKey' => 'user_id',
            'conditions' => array('active' => true),
            'fields' => array('id','username','photo', 'slug'), ),
        );
*/
    public $belongsTo = array('Category');
}
