	<script>
	  $( function() {
		$( "#tabsc" ).tabs();
	  } );
	</script>
	<div id="joinpagehead">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-11 col-sm-4 tittle" style="color:#74568c;">Create a New Carepolis</div>
			</div>
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-11 col-sm-4 subtittle" style="color:#4a4a4a;">It’s easy, free and private</div>
			</div>
		</div>			
	</div>
	<div id="joinpagecontent">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-9 col-sm-4 jointext">
					Your Careopolis is free, private and secure. Only friends you invite can become members of your Careopolis. Each person that you invite will receive an email notification and a unique password allowing them access to your Careopolis. You can remove your Careopolis or anyone's access to it at anytime. 
				</div>
				<div class="col-md-2"></div>
			</div>		
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-8">
				<div id="tabsc">
					<ul>
						<li style="width: 95px; height: 70px;background-image: linear-gradient(to bottom, #e7e7e7, #cccccc);border-top-right-radius: 8px;border-top-left-radius: 8px;"><a href="#1" style="font-size: 28px;font-weight: bold;padding: 20px 36px;">1.</a></li>
						<li style="width: 95px; height: 70px;background-image: linear-gradient(to bottom, #e7e7e7, #cccccc);border-top-right-radius: 8px;border-top-left-radius: 8px;"><a href="#2" style="font-size: 28px;font-weight: bold;padding: 20px 36px;">2.</a></li>
						<li style="width: 95px; height: 70px;background-image: linear-gradient(to bottom, #e7e7e7, #cccccc);border-top-right-radius: 8px;border-top-left-radius: 8px;"><a href="#3" style="font-size: 28px;font-weight: bold;padding: 20px 36px;">3.</a></li>
						<li style="width: 95px; height: 70px;background-image: linear-gradient(to bottom, #e7e7e7, #cccccc);border-top-right-radius: 8px;border-top-left-radius: 8px;"><a href="#4" style="font-size: 28px;font-weight: bold;padding: 20px 36px;">4.</a></li>
						<li style="width: 95px; height: 70px;background-image: linear-gradient(to bottom, #e7e7e7, #cccccc);border-top-right-radius: 8px;border-top-left-radius: 8px;"><a href="#5" style="font-size: 28px;font-weight: bold;padding: 20px 36px;">5.</a></li>
					</ul>
					<?php echo $this->Form->create('User', array('url'=>array('action' => 'register'), 'novalidate' => true));?>
					<div id="1" style="padding: 0em 0em;"  style="background-color: #f4f4f4;">
						<div class="row">
							<div class="bg-tab-title">
								<div class="col-md-1" style="font-size: 32px; font-weight: bold; color: #ffffff; padding-left:20px; padding-top:8px;">WHO</div>
								<div class="col-md-8"></div>
								<div class="col-md-3" style="font-size: 18px;font-weight: 500;text-align: right;color: #ffffff; padding-rigth:15px; padding-top:17px;"><img src="<?php echo $this->Html->url('/img/icon/icon-help-text.png'); ?>"/>&nbsp&nbsp&nbspShow Help</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-4">
								<?php echo $this->Form->input('name', array('label'=> __('Careopolis Name *'), 'size'=>'33'));?>
								<div>If you are a fan of one of these organizations, select it and their logo will appear in your Careopolis*</div>
								<?php
									echo $this->Form->label('City.country_id', __('Country'));
									echo $this->Form->select('City.country_id', $countries);?>
								<div>In this box, please indicate the name of the patient that this Careopolis will be focused on *</div>
								<div class="bg-box-focused_person">
									<?php echo $this->Form->input('end_date_check', array('type'=>'checkbox', 'id'=>'end_date_check', 'label'=>__('Add end date'), 'checked'=>isset($end_date_checked)));
									echo '<div id="end_date_input">';
									echo $this->Form->input('end_date',array(  'dateFormat'=>'DMY',
																'timeFormat'=>Configure::read('evento_settings.timeFormat'),
																'minYear' => date('Y'),
																'maxYear' => date('Y')+5,
																'separator' => "",
																'class'=>'EndDate'
																));
									echo '</div>';?>
								</div>
							</div>
							<div class="col-md-6 col-sm-4">
								<center>
									<img src="<?php echo $this->Html->url('/img/profile-image-default.png'); ?>"/>
									<?php  echo $this->Form->input('filedata', array('type'=>'file', 'label'=>__('Profile photo')));?>
								</center>
							</div>
							<div class="col-md-9 col-sm-4 -You-need-to-enter">* You need to enter all required fields in order to continue. </div>
							<div class="col-md-3 col-sm-4"><div class="bg-buttom labeladdcareo">Save & Next &nbsp&nbsp></div></div>
						</div>
					</div>
					<div id="2" style="padding: 0em 0em;">
						<div class="row">
							<div class="bg-tab-title">
								<div class="col-md-1" style="font-size: 32px; font-weight: bold; color: #ffffff; padding-left:20px; padding-top:8px;">WHAT</div>
								<div class="col-md-8"></div>
								<div class="col-md-3" style="font-size: 18px;font-weight: 500;text-align: right;color: #ffffff; padding-rigth:15px; padding-top:17px;"><img src="<?php echo $this->Html->url('/img/icon/icon-help-text.png'); ?>"/>&nbsp&nbsp&nbspShow Help</div>
							</div>
						</div>
					</div>
					<div id="3" style="padding: 0em 0em;">
						<div class="row">
							<div class="bg-tab-title">
								<div class="col-md-1" style="font-size: 32px; font-weight: bold; color: #ffffff; padding-left:20px; padding-top:8px;">WHERE</div>
								<div class="col-md-8"></div>
								<div class="col-md-3" style="font-size: 18px;font-weight: 500;text-align: right;color: #ffffff; padding-rigth:15px; padding-top:17px;"><img src="<?php echo $this->Html->url('/img/icon/icon-help-text.png'); ?>"/>&nbsp&nbsp&nbspShow Help</div>
							</div>
						</div>
					</div>
					<div id="4" style="padding: 0em 0em;">
						<div class="row">
							<div class="bg-tab-title">
								<div class="col-md-1" style="font-size: 32px; font-weight: bold; color: #ffffff; padding-left:20px; padding-top:8px;">WHY</div>
								<div class="col-md-8"></div>
								<div class="col-md-3" style="font-size: 18px;font-weight: 500;text-align: right;color: #ffffff; padding-rigth:15px; padding-top:17px;"><img src="<?php echo $this->Html->url('/img/icon/icon-help-text.png'); ?>"/>&nbsp&nbsp&nbspShow Help</div>
							</div>	
						</div>	
					</div>
					<div id="5" style="padding: 0em 0em;">
						<div class="row">
							<div class="bg-tab-title">
								<div class="col-md-1" style="font-size: 32px; font-weight: bold; color: #ffffff; padding-left:20px; padding-top:8px;">SETTINGS</div>
								<div class="col-md-8"></div>
								<div class="col-md-3" style="font-size: 18px;font-weight: 500;text-align: right;color: #ffffff; padding-rigth:15px; padding-top:17px;"><img src="<?php echo $this->Html->url('/img/icon/icon-help-text.png'); ?>"/>&nbsp&nbsp&nbspShow Help</div>
							</div>	
						</div>
						<div>
							<input type="submit" class="btn btn-primary bnt-login" value="Register Now" />
						</div>						
					</div>
				</div>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
