	<!-- start contact -->
		<div id="contact">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-4 justify-content-left message wow fadeInLeft" data-wow-delay="0.6s">
						<h1>CareFlash Offers The Careopolis</h1>
						<h2>- A Metropolis of Love and Empathy</h2>
						  <br />
						  <div class="font-weight-light">
							<span class="banner-description">A Careopolis is a Private, Invitation-only Website. It Engages<br />
							  Friends and Family Members around a Loved One in regard to<br />
							  any Health, Wellness or Aging Process or Event.            </span>
						  </div>
						  <br />
						  <div class="buttons-banner">
							<a class="btn btn-primary bnt-create-care" href="/site/signin" role="button">Create a Careopolis</a>
							<a class="btn btn-primary bnt-tell-friend" href="javascript:cf_links.tellfriend();" role="button">Tell a Friend</a>
						  </div>
					</div>
					<?php if ($this->Session->read('Auth.User')) {?>
						<div class="wow fadeIn" data-wow-delay="0.9s">
							<img src="<?php echo $this->Html->url('/img/col-7-bubble-images.png'); ?>" class="img-responsive"/>
						</div>
					<?php } else { ?>
					<div class="banner-image col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
						<img src="<?php echo $this->Html->url('/img/4-col-bubble-images.png'); ?>" class="img-responsive"/>
					</div>
					<div class="col-md-3 col-sm-4 justify-content-left">
					  <div class="container-login">
						<h3>Already a Careopolitan?</h3>
							<!--<form action="#" method="post" id="contact-form" role="form">
							<form action="/careflashpro/users/login" novalidate="novalidate" id="UserLoginForm" method="post" accept-charset="utf-8">-->
							<?php
								echo $this->element('facebook_login');
								echo $this->Session->flash('auth');
								echo $this->Form->create('User', array('novalidate'=>true, 'url'=>array('controller'=>'users', 'action' => 'login')
									, 'id'=>'UserLoginForm'));?>
								<div><span style="padding-left:17px;">Username or Email:</span></div>
								<div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
									<input name="data[User][email]" type="text" id="UserEmail" maxlength="100" required="required" type="email" placeholder="Username or Email" autocomplete="on">
								</div>
								<div><span style="padding-left:17px;">Password</span></div>
								<div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.6s">
									<input name="data[User][password]" type="password" id="UserPassword" required="required" placeholder="Password">						
								</div>
								<div style="padding-left:17px;">
								  <input type="checkbox" name="rememberMe" id="rememberMe" />
								  <label style="font-size: 12px;font-weight: 100;">Remember me</label>
								</div>
								</br>
								<div style="padding-left:17px;">
								  <input type="submit" class="btn btn-primary bnt-login" value="Login" />
								  <a class="forgot-password" href="/evento/users/recover">Forgot your password?</a>
								</div>
								<div style="padding-left:17px;">
								  <a class="signin-socialnetwork" href="/social/signIn?network=Facebook&eventcb=user.socialSignin">
									<span class="icon-facebook rounded-circle">
									  <i class="fa fa-fw fa-facebook-f"></i>
									</span>
									<label class="sign-in-fa">Sign in with Facebook</label>
								  </a>
								</div>
							</form>					  						
					  </div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- start divider -->
		<div id="divider">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-2"></div>
					<div class="col-md-8 col-sm-8">
						<h2 class="wow bounce">Your Careopolis :D</h2>
						<h3 class="wow fadeIn" data-wow-delay="0.6s">It is private, easy to create and use, mobile-friendly, multilingual and free.</h3>
						<!--<p class="wow fadeInUp" data-wow-delay="0.9s">Nulla ultricies bibendum augue et molestie. Suspendisse pellentesque mollis imperdiet. Quisque sodales laoreet tincidunt. Phasellus ut mi orci. Vivamus id odio ac justo tincidunt placerat. Nulla facilisi. Vivamus et dolor urna. Sed vestibulum urna justo, nec malesuada urna aliquet et.</p>-->
					</div>
					<div class="col-md-2 col-sm-2"></div>
				</div>
			</div>
		</div>
		<!-- end divider -->
		
		<!-- start team -->
		<div id="team">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
						 <div class="feature-item">
							<div class="icon-big icon-connect"></div>
							<div class="feature-description">Foster a supportive fabric of connectedness</div>
						  </div>
					</div>
					<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
						  <div class="feature-item">
							<div class="icon-big icon-heart"></div>
							<div class="feature-description">Empowered with empathy, more than just sympathy</div>
						  </div>
					</div>
					<div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
						 <div class="feature-item">
							<div class="icon-big icon-people"></div>
							<div class="feature-description">Part of the journey, as opposed to more of the landscape</div>
						  </div>
					</div>
				</div>
			</div>
		</div>
		<!-- end team -->

		<!-- start about -->
		<div id="about">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-delay="0.9s" style="padding-right:0px;"><img src="<?php echo $this->Html->url('/img/6-col-image-section.png'); ?>" class="img-responsive"/></div>
					<div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-delay="0.9s">
							<div class="featuresContent-items">
							  <h2>Features &amp; Content</h2>
							  <div class="col-md-6 col-sm-6">
								<div class="feature-list">
								  <ul>
									<li>Community blog</li>
									<li>Interactive calendar</li>
									<li>Storytelling tool</li>
									<li>Photo sharing</li>
									<li>3D health animations</li>
									<li>Video sharing</li>
									<li>Coordinates &amp; engages</li>
								  </ul>
								</div>
							  </div>
							  <div class="col-md-6 col-sm-6">
								<div class="feature-list">
								  <ul>
									<li>Mobile friendly</li>
									<li>Multilingual</li>
									<li>Invitation only</li>
									<li>Private and secured</li>
									<li>No ads</li>
									<li>Crowdfunding</li>
								  </ul>
								</div>
							  </div>
							</div>
							<div class="take-tour">
							  <a href="/tour">TAKE TOUR <img src="<?php echo $this->Html->url('/img/icon/icon-arrows.png'); ?>"/></a>
							</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end about -->

		<!-- start newsletter -->
		<div id="newsletter">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<div class="middle-banner-tittle">Community when it matters most</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="start-Community">
							<a class="btn btn-secondary btn-lg" href="/site/signin" role="button">Start a Careopolis Now</a>
							<span class="careopolis-free">It's FREE!</span>
						 </div>
					</div>
				</div>
			</div>
		</div>
		<!-- end newsletter -->
		 <!-- Video Section -->
		  <section class="video-section" id="videoSection">
			<div class="container">
			  <div class="row">
				<div class="col-md-6 col-sm-6 video-containe wow fadeInLeft" data-wow-delay="0.9s">
				  <div style="padding-top:60px">
					<h3>Learn about Careopolis in video</h3>
				  </div>
				  <div class="video-container">
				  <!--<video width="475" height="268" controls poster="/img/icon/play-video-screen-control.png"><source src="/media/Careopolis and Social Healing.mp4" type="video/mp4"></video>-->
				  <video width="475" height="268" controls poster="<?php echo $this->Html->url('/img/image-video-cover.jpg'); ?>"><source src="<?php echo $this->Html->url('/media/Careopolis and Social Healing.mp4'); ?>" type="video/mp4"></video>
				  <!--<object width="475" height="288" data="https://www.youtube.com/v/-1JZOd0BnLM"></object>
				  
				  <!--<video width="475" height="268" controls poster="<?php echo $this->Html->url('/img/icon/play-video-screen-control.png'); ?>"><source src="https://www.youtube.com/embed/-1JZOd0BnLM"></video>
				  <iframe width="475" height="268" src="https://www.youtube.com/embed/-1JZOd0BnLM" frameborder="0"></iframe>--></div>
				  
				</div>
				<div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-delay="0.9s"><img src="<?php echo $this->Html->url('/img/6-col-image-section_2.png'); ?>" class="img-responsive"/></div>
			  </div>
			</div>
		  </section>

		  <!-- Partner Section -->
		  <section class="partner-section" id="partnerSection">
			<div class="container">
			  <div class="row">
				<div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-delay="0.9s" style="padding-right:0px;"><img src="<?php echo $this->Html->url('/img/col-6-image-section.png'); ?>" class="img-responsive"/></div>
				<div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-delay="0.9s">
						<div class="featuresContent-items">
							  <h2>CareFlash also partners with organizations to:</h2>
							  <div class="col-md-6 col-sm-6">
								<div class="feature-list">
								  <ul>
									<li>Rally loved ones in 'whole healing'</li>
									<li>Enhance outlook and optimism</li>
									<li>Increase patient satisfaction</li>
								  </ul>
								</div>
							  </div>
							  <div class="col-md-6 col-sm-6">
								<div class="feature-list">
								  <ul>
									<li>Improve family experience</li>
									<li>Enhance outcomes</li>
									<li>Expand marketing</li>
								  </ul>
								</div>
							  </div>
						</div>	
							<div class="take-tour">
							  <a href="/tour">LEARN MORE <img src="<?php echo $this->Html->url('/img/icon/icon-arrows.png'); ?>"/></a>
							</div>
											
				</div>
			  </div>
			</div>
		  </section>