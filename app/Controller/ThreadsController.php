<?php
class ThreadsController extends AppController
{
    public $name = 'Threads';


    public function obtenthreads($id = null)
    {
		/*return $this->Careopoli->find('all', array('conditions' => array('Careopoli.id' => $id)));*/
		$threads = $this->Thread->find('all', array('conditions' => array('Thread.parent' => $id), 'order' => 'Thread.createDate DESC'));
		//pr($threads);
		return $threads;
		/*$this->set('careopoli',$careopoli);*/
    }
	
	public function admin_index()
    {
        $this->set('threads', $this->Thread->find('all', array(
            'order' => 'Threads.title',
            'fields' => array('Thread.title', 'Thread.id'), )
        ));
        $this->set('title_for_layout', __('Manage Threads'));
    }

    public function admin_add()
    {
        if (!empty($this->request->data) && $this->Thread->save($this->request->data)) {
            $this->redirect(array('action' => 'index'));
        }
        $this->set('title_for_layout', __('Add a new Thread'));
    }


    public function admin_edit($categoryId = null)
    {
        if (!$categoryId) {
            throw new NotFoundException();
        }
        if (!empty($this->request->data)) {
            if ($this->Thread->save($this->request->data)) {
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $thread = $this->Thread->find('first', array('conditions' => array('id' => $categoryId),
                'fields' => array('Thread.id', 'Thread.title'), ));
            if (empty($thread)) {
                throw new NotFoundException();
            }
            $this->request->data = $thread;
        }
        $this->set('title_for_layout', __('Edit Thread'));
    }

    public function admin_delete($categoryId = null, $confirmation = false)
    {
        if (!$categoryId) {
            throw new NotFoundException();
        }
        $this->Thread->Post->recursive = -1;
        $posts = $this->Thread->Post->find('all', array(
            'conditions' => array('Post.thread_id' => $categoryId),
            'fields' => array('Post.id'),
        ));

        if ((!empty($posts) && $confirmation != false) || empty($posts)) {
            $this->Thread->Post->bulkDelete(Set::extract('/Post/id', $posts));
            $this->Thread->delete($categoryId);
            $this->redirect(array('action' => 'index'));
        }
        $this->set('id', $categoryId);
        $this->set('posts', count($posts));
    }

    public function admin_merge($categoryId = null, $categorySlug = null)
    {
        if ($categoryId === null || $categorySlug === null) {
            throw new NotFoundException();
        }
        $this->Thread->merge($categoryId, $categorySlug);
        $this->redirect(array('action' => 'index'));
    }

    public function admin_search()
    {
        $this->set('title_for_layout', __('Search'));
        $data = $this->Session->read('Search.term');
        if (!empty($this->request->data) || !empty($data)) {
            if ($this->request->data) {
                $this->Session->write('Search.term', $this->request->data);
            } else {
                $this->request->data = $data;
            }
            $conditions = array('Thread.name like' => '%'.$this->request->data['Search']['thread'].'%');
            $this->set('threads', $this->paginate('Thread', $conditions));
        }
        $this->render('admin_index');
    }

    public function admin_bulk()
    {
        if (empty($this->request->data)) {
            throw new NotFoundException();
        }
        $ids = array();
        foreach ($this->request->data['Thread']['id'] as $key => $value) {
            if ($value) {
                $ids[] = $key;
            }
        }
        $this->Thread->bulkDelete($ids);
        $this->redirect(array('action' => 'index'));
    }
}
