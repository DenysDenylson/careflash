	<div id="careopolihead">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="imagen"><img src="<?php echo $this->Html->url('/img/icon/col-3-careopolis-main-image.png'); ?>"/></div>	
				</div>
				<div class="col-md-8 col-sm-8">
					<div class="description">
								<div class="col-4-Careopolis-name"><?php echo $careopoli['Careopoli']['title'];?></div>
								<div class="col-4-What-is-going-text"><?php echo $careopoli['Careopoli']['content'];?></div>
								<div class="col-Careopolis-direcction"><?php echo $careopoli['Careopoli']['location'];?></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 bgbocareopolihead"></div>
			</div>
		</div>
	</div>
	<div id="careopolibody">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="leftbar">	
						<div class="careopolisoptions">EDIT CAREOPOLIS</div>
						<div class="bg-header-sidebar">Focus of this Careopolis</div>
						</br></br></br>
						<div class="divider"></div>
						</br>
						<div class="Get-help-to-start-a">Get help to start a fundraising campaing</div>
						</br>
						<img src="<?php echo $this->Html->url('/img/icon/crowdrise.png'); ?>" style="margin-left:40px; margin-right:9px; margin-top:21px; margin-bottom:24px;"/><img src="<?php echo $this->Html->url('/img/icon/icon-arrows-orange.png'); ?>"/>
						</br>
						<div class="bg-header-sidebar">Careopolis Members</div>
						<div class="row">
							<ul>
							<?php foreach($careopoli['Careo'] as $member){ ?>
								<li>
									<div class="col-md-2">
									<?php if($member['photo']=='user_photo.jpg'){ echo $this->Html->image('users/user-avatar-default.png');} 
											else{ echo $this->Html->image('users/'.$this->Session->read('Auth.User.photo'), array('class'=>'imground'));}?>
									</div>
									<div class="col-md-10">
										<div class="username"><?php echo  ucwords($member['firstname']).' '.ucwords($member['lastname']);?></div>
											<div class="role">
												<?php if($member['CareopolisUser']['isModerator']==1){echo 'ADMINISTRATOR';
												} else { echo 'PARTICIPANT';} ?>
											</div>
										</div>
								</li>
							<?php } ?> 
							</ul>
						</div>	
					</div>
				</div>
				<div class="col-md-8">					
					<!--<div class="content"></div>-->
					<div class="careopoliconte">		

							<div class="row">
								<div id="tabs">
									<ul>
										<li><a href="#discussion"><img src="<?php echo $this->Html->url('/img/icon/careopolis_ac_discussion.png'); ?>"/> Discussion</a></li>
										<li><a href="#calendar"><img src="<?php echo $this->Html->url('/img/icon/careopolis_in_calendar.png'); ?>"/> iHelp Calendar</a></li>
										<li><a href="#photos"><img src="<?php echo $this->Html->url('/img/icon/careopolis_in_photos.png'); ?>"/> Photos</a></li>
										<li><a href="#stroytelling"><img src="<?php echo $this->Html->url('/img/icon/careopolis_in_story.png'); ?>"/> Storytelling</a></li>
										<li><a href="#infvideos"><img src="<?php echo $this->Html->url('/img/icon/careopolis_in_videos.png'); ?>"/> Info Videos</a></li>
									</ul>
									<div id="discussion">
										<div class="container">
												<?php $threads = $this->requestAction('threads/obtenthreads/'.$careopoli['Careopoli']['boardid']);
														if(!empty($threads)){ 
														foreach($threads as $thread){
														$author = $this->requestAction('users/obtenmember/'.$thread['Thread']['author']);?>
														<div class="row itemcareopoli">
															<div class="col-md-2 col-sm-2 discussionleft">
																<div>
																	<?php if($author[0]['User']['photo']=='user_photo.jpg'){ echo $this->Html->image('users/user-avatar-default.png',array(width =>'80px',height=>'80px'));} 
																	else{ echo $this->Html->image('users/'.$this->Session->read('Auth.User.photo'), array('class'=>'imground'));}?>
																</div>
																<div class="Posted-by">Posted by</div>
																<div class="namemember"><?php echo ucwords($author[0]['User']['firstname']).' '.ucwords($author[0]['User']['lastname']);?></div>
																<div class="date">	
																	<?php $date=date_create($thread['Thread']['createDate']);
																	echo date_format($date,'M\, j Y g:i a');?>
																</div>
															</div>
															<div class="col-md-10 col-sm-10">
																<div class="discussioncontenpost">
																	<div class="discussion-title"><?php echo $this->Html->link($thread['Thread']['title'],array('controller' => 'careopolis','action' => 'view',$careopolitem[0]['Careopoli']['title']));?></div>
																	<?php $reactions = $thread['Thread']['postcount']-1 ?>
																	<div class="views-answers-counter"><?php echo $thread['Thread']['viewcount'].' views and ',$reactions.' reactions';?></div>
																	<div class="body-text"><?php echo $thread['Thread']['description'];?></div>
																</div>
																<div class="discussioncontenpostdivi"></div>
																<div class="discussioncontenpostdetails">
																	<div class="row">
																		<div class="col-md-6 col-sm-6 last-answer-date-entry">Last reaction on Jun 08, 4:32 PM</div>
																		<div class="col-md-6 col-sm-6 labeldet"><center>View Reactions & Add Yours <img src="<?php echo $this->Html->url('/img/icon/icon-arrows-dark-green.png'); ?>"/></center></div>
																	</div>
																</div>
															</div>
														</div>
												<?php } } ?>
										</div>
									</div>
									<div id="calendar">
										<div class="container">
											<div class="row">
												
											</div>
										</div>
									</div>
									<div id="photos">
										<div class="container">
											<div class="row">
												
											</div>
										</div>
									</div>
									<div id="stroytelling">
										<div class="container">
											<div class="row">
												
											</div>
										</div>
									</div>	
									<div id="infvideos">
										<div class="container">
											<div class="row">
												
											</div>
										</div>
									</div>
								</div>
							</div>

					</div>
				</div>
			</div>
		</div>			
	</div>
	
		
