<?php
class PostsController extends AppController
{
   public $name = 'Posts';


    public function admin_index()
    {
        $this->set('post', $this->Post->find('all', array(
            'order' => 'Post.title',
            'fields' => array('Post.title', 'Post.id'), )
        ));
        $this->set('title_for_layout', __('Manage Post'));
    }

    public function admin_add()
    {
        if (!empty($this->request->data) && $this->Post->save($this->request->data)) {
            $this->redirect(array('action' => 'index'));
        }
        $this->set('title_for_layout', __('Add a new Post'));
    }


    public function admin_edit($categoryId = null)
    {
        if (!$categoryId) {
            throw new NotFoundException();
        }
        if (!empty($this->request->data)) {
            if ($this->Post->save($this->request->data)) {
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $post = $this->Post->find('first', array('conditions' => array('id' => $categoryId),
                'fields' => array('Post.id', 'Post.title'), ));
            if (empty($post)) {
                throw new NotFoundException();
            }
            $this->request->data = $post;
        }
        $this->set('title_for_layout', __('Edit Post'));
    }
/*
    public function admin_delete($categoryId = null, $confirmation = false)
    {
        if (!$categoryId) {
            throw new NotFoundException();
        }
        $this->Thread->Post->recursive = -1;
        $posts = $this->Thread->Post->find('all', array(
            'conditions' => array('Post.thread_id' => $categoryId),
            'fields' => array('Post.id'),
        ));

        if ((!empty($posts) && $confirmation != false) || empty($posts)) {
            $this->Thread->Post->bulkDelete(Set::extract('/Post/id', $posts));
            $this->Thread->delete($categoryId);
            $this->redirect(array('action' => 'index'));
        }
        $this->set('id', $categoryId);
        $this->set('posts', count($posts));
    }

    public function admin_merge($categoryId = null, $categorySlug = null)
    {
        if ($categoryId === null || $categorySlug === null) {
            throw new NotFoundException();
        }
        $this->Thread->merge($categoryId, $categorySlug);
        $this->redirect(array('action' => 'index'));
    }
*/
    public function admin_search()
    {
        $this->set('title_for_layout', __('Search'));
        $data = $this->Session->read('Search.term');
        if (!empty($this->request->data) || !empty($data)) {
            if ($this->request->data) {
                $this->Session->write('Search.term', $this->request->data);
            } else {
                $this->request->data = $data;
            }
            $conditions = array('Post.name like' => '%'.$this->request->data['Search']['post'].'%');
            $this->set('posts', $this->paginate('Post', $conditions));
        }
        $this->render('admin_index');
    }

    public function admin_bulk()
    {
        if (empty($this->request->data)) {
            throw new NotFoundException();
        }
        $ids = array();
        foreach ($this->request->data['Post']['id'] as $key => $value) {
            if ($value) {
                $ids[] = $key;
            }
        }
        $this->Post->bulkDelete($ids);
        $this->redirect(array('action' => 'index'));
    }
}
