<?php foreach ($careopolis as $careopoli): ?>
	<div class="careopoli">	
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="border-car base-with-shadow-car"><img src="<?php echo $this->Html->url('/img/icon/col-3-careopolis-main-image.png'); ?>" class="img-responsive"/></div>
				</div>
				<div class="col-md-8 col-sm-8 margeizq">
					<div class="row">
						<div class="h3-careopolis_name col-md-6 col-sm-6"><?php echo $careopoli['Careopoli']['title'];?></div>
						<div class="h3-careopolis_name col-md-6 col-sm-6" style="text-align:right">
						<a href="<?php echo $this->Html->url(array('controller' => 'careopolis', 'action'=>'view', $careopoli['Careopoli']['title'])); ?>" class="btn btn-primary bnt-create-care" role="button">Visit Careopolis</a>
						</div>
					</div>
					<div class="col-7-p-text"><?php echo $careopoli['Careopoli']['title'];?></div>
					<div class="options">
						<div class="row">
							<div class="col-md-5 col-sm-5">
								<img src="<?php echo $this->Html->url('/img/icon/myfeed_discussion.png'); ?>"/><span class="title">Discussion</span>
							</div>
							<div class="col-md-7 col-sm-7 section_activity">
								<?php if($careopoli['Careopoli']['title'] == "Joe Morgan") { ?>
									<div class="link"><?php echo $careopoli['Careopoli']['title'];?></div>
									<div class="info"><?php echo $careopoli['Careopoli']['title'];?></div>
								<?php } else { ?>
									<div class="infonot">No update available</div>
								<?php } ?>
								<div class="divider"></div>
							</div>						
						</div>
					</div>
					<div class="options">
						<div class="row">
							<div class="col-md-5 col-sm-5">
								<img src="<?php echo $this->Html->url('/img/icon/myfeed_photos.png'); ?>"/><span class="title">Photos</span>
							</div>
							<div class="col-md-7 col-sm-7 section_activity">
								<?php if($careopoli['Careopoli']['title'] == "Joe Morgan") { ?>
									<div class="link"><?php echo $careopoli['Careopoli']['title'];?></div>
									<div class="info"><?php echo $careopoli['Careopoli']['title'];?></div>
								<?php } else { ?>
									<div class="infonot">No update available</div>
								<?php } ?>
								<div class="divider"></div>
							</div>
						</div>
					</div>
					<div class="options">
						<div class="row">
							<div class="col-md-5 col-sm-5">
								<img src="<?php echo $this->Html->url('/img/icon/myfeed_calendar.png'); ?>"/><span class="title">iHelp Calendar</span>
							</div>
							<div class="col-md-7 col-sm-7 section_activity">
								<?php if($careopoli['Careopoli']['title'] == "Joe Morgan") { ?>
									<div class="link"><?php echo $careopoli['Careopoli']['title'];?></div>
									<div class="info"><?php echo $careopoli['Careopoli']['title'];?></div>
								<?php } else { ?>
									<div class="infonot">No update available</div>
								<?php } ?>
								<div class="divider"></div>
							</div>
						</div>
					</div>
					<div class="options">
						<div class="row">
							<div class="col-md-5 col-sm-5">
								<img src="<?php echo $this->Html->url('/img/icon/myfeed_story.png'); ?>"/><span class="title">Storytelling</span>
							</div>
							<div class="col-md-7 col-sm-7 section_activity">
								<?php if($careopoli['Careopoli']['title'] == "Joe Morgan") { ?>
									<div class="link"><?php echo $careopoli['Careopoli']['title'];?></div>
									<div class="info"><?php echo $careopoli['Careopoli']['title'];?></div>
								<?php } else { ?>
									<div class="infonot">No update available</div>
								<?php } ?>
								<div class="divider"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>	
<?php endforeach; ?>
