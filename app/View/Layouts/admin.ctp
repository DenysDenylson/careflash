﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
	<meta name="description" content="<?php echo $pageDescription; ?>" />
	<meta name="keywords" content="<?php echo $pageKeywords; ?>" />
    <title><?php echo $title_for_layout; ?></title>
	<!--<link href="<?php //echo $html->url('/favicon.ico'); ?>" type="image/x-icon" rel="icon" />
	<link href="<?php //echo $html->url('/favicon.ico'); ?>" type="image/x-icon" rel="shortcut icon" />-->
   
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <?php
	echo $this->Html->meta('icon');
	echo $this->Html->css(array('admin/font-awesome.min' ,'admin/bootstrap.min', 'admin/templatemo-style', 'admin/jquery-ui-1.8.4.custom'));
	?>

</head>
<body>  
    <!-- Left column -->
    <div class="templatemo-flex-row">
		<div class="templatemo-sidebar">
			<header class="templatemo-site-header">
				<img src="<?php echo $this->Html->url('/img/admin/logo_admin.png'); ?>" class="img3"/>
			</header>    
			<div class="mobile-menu-icon">
				<i class="fa fa-bars"></i>
			</div>
			<nav class="templatemo-left-nav">          
				<?php if($usuarioActual['Usuario']['rol'] == 'Operario'): ?>
					<ul>
			            <li><a href="/admin/monitoreos/inicio"><i class="fa fa-home fa-fw"></i>Inicio</a></li>
			            <!--<li><i class="fa fa-bar-chart fa-fw"></i> Control Economico</li>
						<li><i class="fa fa-cubes fa-fw"></i> Control de Equipos</li>
						<li><i class="fa fa-database fa-fw"></i> Control de Doc.</li>
				        <li><i class="fa fa-users fa-fw"></i>Clientes</li>
				        <li><i class="fa fa-cog fa-fw"></i>Configuraciones</li>-->
						<li><a href="/admin/usuarios/salir"><i class="fa fa-sign-out"></i>Salir</a></li>   
					</ul>
				<?php endif; ?>	
				<?php if($usuarioActual['Usuario']['rol'] == 'Externo'): ?>
					<ul>
			            <li><a href="/admin/usuarios/inicio"><i class="fa fa-home fa-fw"></i>Inicio</a></li>
						<li><a href="/admin/cotizacions/"><i class="fa fa-database fa-fw"></i>Cotizaciones</a></li>
				        <li><a href="/admin/clients/"><i class="fa fa-users fa-fw"></i>Clientes</a></li>
				        <li><a href="/admin/usuarios/miview"><i class="fa fa-cog fa-fw"></i>Configuraciones</a></li>
						<li><a href="/admin/usuarios/salir"><i class="fa fa-sign-out"></i>Salir</a></li>   
					</ul>
				<?php endif; ?>	
				<?php if($usuarioActual['Usuario']['rol'] == 'Gerencial' OR $usuarioActual['Usuario']['rol'] == 'Administrador'): ?>
					<ul>
			            <li><a href="/admin/monitoreos/inicio"><i class="fa fa-home fa-fw"></i>Inicio</a></li>
			            <li><a href="#"><i class="fa fa-bar-chart fa-fw"></i> Control Economico</a></li>
						<li><a href="#"><i class="fa fa-cubes fa-fw"></i> Control de Equipos</a></li>
						<li><a href="/admin/cotizacions/"><i class="fa fa-database fa-fw"></i> Control de Doc.</a></li>
				        <li><a href="/admin/clients/"><i class="fa fa-users fa-fw"></i>Clientes</a></li>
				        <li><a href="/admin/contenidos/configuraciones/"><i class="fa fa-cog fa-fw"></i>Configuraciones</a></li>
						<li><a href="/admin/usuarios/salir"><i class="fa fa-sign-out"></i>Salir</a></li>   
					</ul>
				<?php endif; ?>							
			</nav>
			<!--</br>
			<center>
				<p><b>SISTEMA DE CONTROL ADMINISTRATIVO</b> Derechos Reservados &copy; <?php echo date("Y");?> | <b>TANO</b></p>
			</center>-->
		</div>
		<!-- Main content --> 
		<div class="templatemo-content col-1 light-gray-bg">
			<div class="templatemo-top-nav-container">
				<div class="row">
					<!--<img src="<?php echo $html->url('/img/profile-photo1.png'); ?>" class="img3"/>-->
					<nav class="templatemo-top-nav col-lg-12 col-md-12">
						<?php if (isset($usuarioActual)): ?>
						<ul class="text-uppercase">
							<li><a href="/admin/usuarios/salir"><i class="fa fa-sign-out"></i>Salir</a></li>
							<li><a href="/admin/usuarios/miview"><i class="fa fa-user fa-fw"></i><?php echo $usuarioActual['Usuario']['nombre'];?></a></li>
						</ul>
						<?php endif; ?>			  
					</nav> 
				</div>
			</div>
			<div class="templatemo-content-container">
				<?php $session->flash(); ?>
				<?php echo $content_for_layout; ?>             
				<footer class="text-right">
		            <p><b>SISTEMA DE CONTROL ADMINISTRATIVO</b> Derechos Reservados &copy; <?php echo date("Y");?> | <b>TANO</b></p>
		        </footer>
			</div>
		</div>
    </div>
    
    <!-- JS -->
	<?php echo $this->Html->script(array('admin/basico', 'admin/jquery-1.11.2.min', 'admin/jquery-migrate-1.2.1.min', 'admin/bootstrap-filestyle.min', 'admin/templatemo-script'));?>

</body>
</html>