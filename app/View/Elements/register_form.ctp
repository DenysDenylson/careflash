<?php echo $this->Form->create('User', array('url'=>array('action' => 'register'), 'novalidate' => true));?>
	<div class="row">
		<div class="col-md-1 col-sm-4">
			<?php 
				echo $this->Form->input('prefix ', array('label'=> __('Prefix'), 'autofocus'=>true, 'size'=>'4'));?>
		</div>
		<div class="col-md-5 col-sm-4">
			<?php echo $this->Form->input('first_name', array('label'=> __('First Name *'), 'size'=>'33'));?>
		</div>
		<div class="col-md-4 col-sm-4">
			<?php echo $this->Form->input('last_name', array('label'=> __('Last Name *'), 'size'=>'33'));?>
		</div>
		<div class="col-md-1 col-sm-4"></div>
		<div class="col-md-1 col-sm-4" style="margin-left:-17px">
			<?php echo $this->Form->input('sufix', array('label'=> __('Sufix'), 'size'=>'4'));?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-4">
			<?php echo $this->Form->input('email', array('label'=> __('Email Address *'), 'size'=>'41'));?>
		</div>
		<div class="col-md-6 col-sm-4">
			<?php echo $this->Form->input('email', array('label'=> __('Confirm Email Address *'), 'size'=>'41'));?>
		</div>
	</div>
	<div class="row">	
		<div class="col-md-6 col-sm-4">
			<?php
			echo $this->Form->label('City.country_id', __('Country'));
			echo $this->Form->select('City.country_id', $countries);?>
		</div>
		<div class="col-md-3 col-sm-4">
			<?php
			echo $this->Form->error('City.country_id');
			echo $this->Form->input('City.name', array('id'=>'CityName', 'label'=>__('City'), 'size'=>'20'));?>
			<div id="CityName_autoComplete" class="auto_complete"></div>
		</div>
		<div class="col-md-3 col-sm-4">
			<?php echo $this->Form->input('zip', array('label'=> __('ZIP code (or none)'), 'size'=>'20'));?>
		</div>
	</div>
	<div class="row">	
		<div class="col-md-6 col-sm-4">
			<?php
			echo $this->Form->label('City.country_id', __('Animation language'));
			echo $this->Form->select('City.country_id', $countries);?>
		</div>
		<div class="col-md-3 col-sm-4">
			<?php
			echo $this->Form->label('City.country_id', __('Year you were born *'));
			echo $this->Form->select('City.country_id', $countries);?>
		</div>
		<div class="col-md-3 col-sm-4"></div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-4">
			<?php echo $this->Form->input('username', array('label'=> __('Select a username *'), 'size'=>'41'));?>
		</div>
		<div class="col-md-6"></div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-4">
			<?php echo $this->Form->input('password', array('label'=> __('Select a password *'), 'size'=>'41'));?>
		</div>
		<div class="col-md-6 col-sm-4">
			<?php echo $this->Form->input('password', array('label'=> __('Confirm your password *'), 'size'=>'41'));?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-4" style="margin-bottom:30px;">
			<input type="checkbox" name="rememberMe" id="rememberMe" />
			<label style="font-size: 12px;font-weight: 100;">I have read and agree to the Terms of Use.</label>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-4" style="margin-bottom:30px;">
			<img src="<?php echo $this->Html->url('/img/captcha.jpg'); ?>" width="300px" height="74px"/>
		</div>
		<div class="col-md-6 col-sm-4 registernow">
				</br>
				<input type="submit" class="btn btn-primary bnt-login" value="Register Now" />
				<?php //echo $this->Form->end();?>
		</div>
	</div>
	<div class="row">
		<div class="bg-important-notification">
			<div class="col-md-3 col-sm-4 IMPORTANT">IMPORTANT!</div>
			<div class="col-md-9 col-sm-4 To-receive-email-not">To receive email notifications, please do not forget to add the emai address messenger@careflash.com in your list of safe-senders.</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-4 REACTIVATE-A-DEACTIV">REACTIVATE A DEACTIVATED ACCOUNT</div>
		<div class="col-md-6 col-sm-4 HOW-TO-CREATE-OR-EDI">HOW TO CREATE OR EDIT A CAREOPOLIS</div>
	</div>
	<?php			
	/*if(!isset($this->request->params['admin']) || !$this->request->params['admin']) {
		if($useRecaptcha) { 
			echo $this->Recaptcha->display();
			echo $this->Form->error('recaptcha');
		}
		echo $this->Form->end(__('Register'));
	}
	else {
		echo $this->Form->input('group_id', array('type'=>'select', 'options'=>$groups, 'empty'=>false,
			'label'=>__('Users group')));*/
?>
<!--<div class="submit">
<?php /*
	echo $this->Form->submit(__('Save'), array('div'=>false));
	echo '<p class="backlink"> ';
	echo $this->Html->link(__('Cancel'), array('controller'=>'users', 'action'=>'index')
		, array('class'=>'back-button'));
	echo '</p>';
	echo $this->Form->end();*/
?>
	<div class="clear"></div>
</div>-->
<?php //} ?>