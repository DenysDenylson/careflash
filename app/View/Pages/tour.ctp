	<!-- start tourhead -->
		<div id="tourhead">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4 wow fadeInLeft" data-wow-delay="0.6s">
						<center><img src="<?php echo $this->Html->url('/img/imac-image-careflash.png'); ?>" class="img-responsive"/></center>
					</div>
					<div class="col-md-8 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
						<h1>CareFlash is a free service anyone can use to quickly and easily create a private online Careopolis</h1>
						<h2>As in this brief tour, a Careopolis includes capabilities and content for enhancing connectedness and community</h2>
					</div>
				</div>
			</div>
		</div>

	<!-- start tourinfo -->
		<div id="tourinfo">
			<div class="info-section-left"></div>
			<div class="container">
				<div class="row">
						<div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-delay="0.9s">
							<div class="featuresContent-items">
							  <div class="descriptionleft">
								A Careopolis moves loved ones from sympathy to empathy, enhancing outlook and peace of mind.  It removes emotional barriers so loved ones become, and remain naturally connected among:
							  </div>
							  <div class="col-md-6 col-sm-6">
								<div class="feature-list">
								  <ul>
									<li>Illness or injury</li>
									<li>Healing and rehabilitating</li>
									<li>An aging loved one</li>
									<li>Impending loss</li>
									<li>Grief that follows a death</li>
									<li>Grief that follows a death</li>
									<li>Chronic/behavioral illness</li>
								  </ul>
								</div>
							  </div>
							  <div class="col-md-6 col-sm-6">
								<div class="feature-list">
								  <ul>
									<li>Cognitive challenge</li>
									<li>Promoting wellness</li>
									<li>Transitioning to a prosthetic limb</li>
									<li>Addiction recovery</li>
									<li>New family addition</li>
									<li>Crowdfunding</li>
								  </ul>
								</div>
							  </div>
							</div>
					</div>
					<div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-delay="0.9s">
							<div class="featuresContent-items">
							  <div class="descriptionrigth">
								In addition, CareFlash partners with the following types of organizations, using proven, industry-specific solutions that enhance quality of care and foster outreach, retention and growth:
							  </div>
							  <div class="col-md-6 col-sm-6">
								<div class="feature-list">
								  <ul>
									<li>Healthcare</li>
									<li>Home Health/Home Care</li>
									<li>Physical/Occupational Therapy</li>
									<li>Post-Acute</li>
									<li>Chronic/Behavioral Care</li>	
								  </ul>
								</div>
							  </div>
							  <div class="col-md-6 col-sm-6">
								<div class="feature-list">
								  <ul>
									<li>Eldercare/Senior Living</li>
									<li>Hospice/Palliative Care</li>
									<li>Faith-Based</li>
									<li>Pharmacies/PBMs</li>
									<li>Employee Benefits/Payers</li>
									<li>Advocacy/Community</li>
								  </ul>
								</div>
							  </div>
							</div>	
					</div>
				</div>
			</div>
		</div>
		<!-- end about -->

	<!-- start tourpageitems -->
		<div id="tourpageitems">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<center><img src="<?php echo $this->Html->url('/img/tour-item-icon-1.png'); ?>" class="img-responsive"/></center>
					</div>
					<div class="col-md-9 col-sm-4">
						<h3>It's fast & easy to create your own free, secure Careopolis to engage loved ones</h3>
						<div class="description">
							<p>A Careopolis is 100% invitation-only, so you select who you want to participate in what's going on Participants naturally rally around without the typical awkwardness - without waiting to engage via the grapevine.</p>
							<p>A Careopolis has its own blog - not only for updates and well-wishes - but for sustained, healing dialogues. Blogposts can be easily accompanied with photos and videos that go miles to enhance outlook and optimism!</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<center><img src="<?php echo $this->Html->url('/img/tour-item-icon-2.png'); ?>" class="img-responsive"/></center>
					</div>
					<div class="col-md-9 col-sm-4">
						<h3>Have you ever wondered why loved ones keep asking how they can help?</h3>
						<div class="description">
							<p>People want to help, but don't know how, when or where - or what's most needed. No one wants to intrude, is part of the reason loved ones keep asking how they can help.</p>
							<p>A Careopolis has its own interactive calendar - making it a snap to reach-out and enlist help with meals, meds, errands, appointments and respite. This gets everyone on the same page, organized and moving the same direction - helping keep the path to recovery on schedule!</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<center><img src="<?php echo $this->Html->url('/img/tour-item-icon-3.png'); ?>" class="img-responsive"/></center>
					</div>
					<div class="col-md-9 col-sm-4">
						<h3>Social Storytelling celebrates life using loved ones' photos and in their own voices</h3>
						<div class="description">
							<p>Since "celebrating life" is a tired ole saying, we named our newest module Social Storytelling.  It has nothing to do with well-wishes - nor is it a bunch of photos put to music.</p>
							<p>Social Storytelling is an interactive approach to telling the life story of the person at the heart of that Careopolis - thru members photos and actual voices.</p>
							<p>Shared experiences, adventures, memories and voices creates goodwill and benefit that - extending for years and even generations to come!</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<center><img src="<?php echo $this->Html->url('/img/tour-item-icon-4.png'); ?>" class="img-responsive"/></center>
					</div>
					<div class="col-md-9 col-sm-4">
						<h3>Smiling faces of loved ones brighten the day and foster better outlook</h3>
						<div class="description">
							<p>Therapy takes all sorts of forms.  On the photo-sharing page, loved ones can upload and share images.</p>
							<p>These include smiling faces and warm memories - brightening the day and keeping loved ones connected. Engages loved ones from near and far – allowing them to contribute in ways that leverage memories and elicit smiles.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<center><img src="<?php echo $this->Html->url('/img/tour-item-icon-5.png'); ?>" class="img-responsive"/></center>
					</div>
					<div class="col-md-9 col-sm-4">
						<h3>Targeted, world-class educational material removes emotional barriers!</h3>
						<div class="description">
							<p>A Careopolis contains world-class, 3D healthcare animations and content that helps everyone get up to speed on what's going on.</p>
							<p>The animations are plainly and understandably narrated using simple terms that the average person can easily understand. This removes emotional barriers that explain why people have a difficult time being (and staying) engaged!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- end touritems -->
	
	<!-- start newsletter -->
		<div id="newsletter">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-6">
						<div class="tourbottom">
							<span class="careopolis-free">OK, get started!</span>
							<div class="text">CareFlash is free, and quick and easy to use. It simplifies sharing, engaging and supporting. Love and community are powerful medicine... no matter what's going on!</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="start-Community">
							<a class="btn btn-secondary btn-lg" href="/site/signin" role="button">Register Now</a>		
						 </div>
					</div>
				</div>
			</div>
		</div>
		<!-- end newsletter -->