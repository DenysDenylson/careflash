<div id="joinpagehead">
	<div class="container">
		<div class="row">
			<div class="tittle">Become a CareFlash member today</div>
		</div>
		<div class="row">
			<div class="subtittle">It's completely free</div>
		</div>
	</div>			
</div>
<div id="joinpagecontent">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-4 jointext">
			CareFlash offers free, private online communities, called Careopolis(s) that help you and your family to work through difficult challenges and decisions surrounding caring for loved ones in ways that enhance quality of life and drive better outcomes. Careopolis(s) help family care providers maintain balance in their lives while engaging the most important people around a loved one. Click on the circles to the right to learn more about how CareFlash can help you Share, Support, and Understand when it matters most.
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-sm-4 wow fadeInLeft" data-wow-delay="0.3s" style="padding-right:0px;">
				<div class="row">
					<div class="col-md-8 col-sm-4">
						<div class="h4-form-text">Please create an account filling out the information below or connect with your social account.</div>
					</div>
					<div class="col-md-4 col-sm-4"><img src="<?php echo $this->Html->url('/img/icon/button-share-on-facebook.png'); ?>"/></div>
				</div>
				<?php
				if(isset($code)) {
					echo '<p>'.__('We sent you an email with instructions to confirm your email address.').'</p>';
				} else {
				
				?>
				<!--<div class="row">-->
					<div class="col-md-12 col-sm-4 registration">
							<div class="are-required-fields">*Are required fields</div>
							<?php echo $this->element('facebook_login'); ?>
							
							
							<?php echo $this->element('register_form'); ?>
							<div class="clear"></div>
					</div>
				<!--</div>-->
				<?php } ?>
			</div>
			<div class="col-md-4 col-sm-4">
				<center>
					<div class="h4-title-circles">CareFlash helps to:</div>
					<div class="icon-big share wow fadeIn" data-wow-delay="0.3s">SHARE</div>
						<img src="<?php echo $this->Html->url('/img/icon/connector-1.png'); ?>"/>
					<div class="icon-big support wow fadeIn" data-wow-delay="0.6s">SUPPORT</div>
						<img src="<?php echo $this->Html->url('/img/icon/connector-2.png'); ?>"/>
					<div class="icon-big understand wow fadeIn" data-wow-delay="0.9s">UNDERSTAND</div>
				</center>
			</div>
		</div>
	</div>
</div>
